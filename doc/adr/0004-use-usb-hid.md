# 4. Use USB HID

Date: 2020-11-25

## Status

Accepted

## Context

There are at least two possible approaches to programming the micro-controller:

1. Use the Kerbal _simpit_ mod with the game and communicate with the mod via serial.

2. Implement the controller as a USB HID Gamepad.

### Simpit

The mod exposes game data over serial to the microcontroller for a richer experience than can be provided by a simple gamepad.

Ideally, this mod would be available in CKAN (the Kerbal mod package management system), but the author has not pushed a recent build to CKAN. There are instructions to add the package by providing a custom URL to the CKAN CLI. That URL now responds with 404.

After several challenges getting the mod installed on MacOS, I could not get the Arduino to establish a serial connection with the _Simpit_ module.

### USB HID Gamepad

The controller can be exposed to the gaming computer as a standard USB HID (Human Interface Device) gamepad. The controller will not receive any information about the game state, but it will be a standard interaction with the gaming computer and require no installation of additional mods.

## Decision

Implement the controller as a USB HID Gamepad.

## Consequences

The controller will require no extra software to be installed.

In order to use the controller, the user will have to add mappings in the KSP controller settings.

The game state and controller state can become out of sync. For example, if the user turns on RCS via the physical controller and then deactivates it via the keyboard or switches to another vessel where RCS is deactivated, the game state will have RCS activated, but the physical switch will be "on" and the LED will be lit.

