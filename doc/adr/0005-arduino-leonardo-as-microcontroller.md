# 5. Arduino Leonardo as microcontroller

Date: 2020-12-26

## Status

Accepted

## Context

Something needs to translate switch flips and button presses to something the game understands. I could use something like a Raspberry Pi or an Arduino or something even smaller/cheaper.

## Decision

Use an Arduino Leonardo as the micro-controller.

This decisions was based on informal research on [the r/KerbalControllers subreddit](https://www.reddit.com/r/KerbalControllers/) and because _Simpit_ has [a corresponding Arduino library](https://bitbucket.org/pjhardy/kerbalsimpit-arduino).

## Consequences

The microcontroller will be limited to 20 digital I/O pins or 12 digital and 8 analog I/O pins. (Of course, shift registers can be used to overcome this limit if necessary.)

The controller will not need to boot an operating system before becoming useful (vs a Raspberry Pi, for example).

The controller will connect and power over USB. No Bluetooth or WiFi communication will be possible.

