# 3. Wire arming switch and button in serial

Date: 2020-11-26

## Status

Accepted

Supercedes [2. Link arming switches and buttons in software](0002-link-arming-switches-and-buttons-in-software.md)

## Context

See [ADR 2](0002-link-arming-switches-and-buttons-in-software.md).

Further, I figured out how the arming switch works. No schematics accompanied the hardware, so this took trial and error. The contact not labeled ground and not labeled "+" has an image that could represent the three contacts of the switch, or it could represent a bulb illuminating.

Supplying current to this contact (after tying ground low and "+" high) causes the integrated LED to light. However, this is actually the intended output of the switch.

I now have a working prototype with the following schematic:

![schematic](../13-arming-switch-in-serial.png)


## Decision

Wire the switches in series.

## Consequences

This will use fewer pins on the Arduino. There are 20 pins available for digital I/O, so this is not a significant win.

It will be _impossible_ for a software defect to cause the switch and button to operate in an invalid state.

It will not be possible to light the LEDs when the switches are open.

