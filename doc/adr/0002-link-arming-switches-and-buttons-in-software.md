# 2. Link arming switches and buttons in software

Date: 2020-11-21

## Status

Superceded by [3. Wire arming switch and button in serial](0003-wire-arming-switch-and-button-in-serial.md)

## Context

I will use arming switches for the _abort_ and _stage_ actions. The action buttons should be ignored (i.e. pressing the button should have no effect) if the arming switch is open.

There are a couple options:

### 1. Hardware enforcement

Wire the switches in series, so that both must be closed in order to fire the corresponding action (_abort_ or _stage_).

Wire the button LED to the arming switch as well so that it is not lit unless the arming switch is closed.

Wire the arming switch LED in such a way that it is on when the arming switch is closed, independent of the button being depressed.

### 2. Software enforcement

Wire each switch and LED to the Arduino. Control the LEDs and the "and" logic of the two switches in software.


## Decision

Use software enforcement of the two switches rather than hardware. In order for the arming switch's LED to light, the circuit must be closed. Therefor, wiring the button in serial keeps the arming switch LED from lighting until the button is depressed.

## Consequences

This will use more pins on the Arduino. There are 20 pins available for digital I/O, so this should not be a problem.

It will be possible for the switch and button to operate in an invalid state due to a software defect.

It will be possible to light the LEDs when the switches are open. This creates some opportunities for UX improvements. E.g. When the button is pressed, but the arming switch is open, I can blink the arming switch LED to indicate that the button is ineffective and that the arming switch needs to be closed.

