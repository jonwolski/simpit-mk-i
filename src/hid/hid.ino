/* HID
 *
 *  Use Arduino Leonardo as a USB HID. Map button presses and switch toggling to
 *  gamepad buttons.
 */

#include "HID-Project.h"

const int DEBOUNCE_DELAY = 50;
const bool STATE_CHANGED = true;
const bool STATE_DID_NOT_CHANGE = false;

typedef int ButtonState;
typedef int GamePadButton;

struct Button {
  unsigned int pin;
  ButtonState lastState;
  unsigned long lastTransitionTime;
  String name;
};

static struct Button armedButtons[] = {
  { pin: 2, lastState: LOW, lastTransitionTime: 0L, name: "Abort"},
  { pin: 3, lastState: LOW, lastTransitionTime: 0L, name: "Stage"},
};

static struct Button toggleSwitches[] = {
  { pin:  4, lastState: LOW, lastTransitionTime: 0L, name: "RCS"},
  { pin:  5, lastState: LOW, lastTransitionTime: 0L, name: "SAS"},
  { pin:  6, lastState: LOW, lastTransitionTime: 0L, name: "Lights"},
  { pin:  7, lastState: LOW, lastTransitionTime: 0L, name: "Gear"},
  { pin:  9, lastState: LOW, lastTransitionTime: 0L, name: "Custom Action Group 1"},
  { pin: 10, lastState: LOW, lastTransitionTime: 0L, name: "Custom Action Group 2"},
  { pin: 11, lastState: LOW, lastTransitionTime: 0L, name: "Custom Action Group 3"},
  { pin: 12, lastState: LOW, lastTransitionTime: 0L, name: "Custom Action Group 4"},
  { pin: 13, lastState: LOW, lastTransitionTime: 0L, name: "Custom Action Group 5"},
};

static struct Button momentSwitches[] = {
  { pin:  8, lastState: LOW, lastTransitionTime: 0L, name: "Brake"},
};

/* returns true if button changed state ignoring bounces */
static bool debounce(Button &button) {
  int currentState = digitalRead(button.pin);
  unsigned long currentTick = millis();

  if ((currentTick - button.lastTransitionTime) > DEBOUNCE_DELAY) {
    button.lastTransitionTime = currentTick;
    if (currentState != button.lastState) {
      button.lastState = currentState;
      return STATE_CHANGED;
    }
  }
  return STATE_DID_NOT_CHANGE;
}


void setup() {
  Serial.begin(9600);
  for (auto & button : armedButtons) {
    pinMode(button.pin, INPUT);
  }
  for (auto & button : toggleSwitches) {
    pinMode(button.pin, INPUT);
  }
  for (auto & button : momentSwitches) {
    pinMode(button.pin, INPUT);
  }

//  Serial.println("beginning gamepad");
  Gamepad.begin();
//  Serial.println("setup complete");
}

void loop() {

  for (auto & button : armedButtons) {
    if (debounce(button) == STATE_CHANGED) {
      if (button.lastState == HIGH) {
//        Serial.print(button.name);
//        Serial.println(" button is pressed");
        Gamepad.press(button.pin);
        Gamepad.write();
      } else {
//        Serial.print(button.name);
//        Serial.println(" button is released");
        Gamepad.release(button.pin);
        Gamepad.write();
      }
    }
  }

  for (auto & button : toggleSwitches) {
    if (debounce(button) == STATE_CHANGED) {
//      Serial.print(button.name);
//      Serial.println(" button is pressed");
      Gamepad.press(button.pin);
      Gamepad.write();
//      Serial.print(button.name);
//      Serial.println(" button is released");
      Gamepad.release(button.pin);
      Gamepad.write();
    }
  }

  for (auto & button : momentSwitches) {
    if (debounce(button) == STATE_CHANGED) {
      if (button.lastState == HIGH) {
//        Serial.print(button.name);
//        Serial.println(" button is pressed");
        Gamepad.press(button.pin);
        Gamepad.write();
      } else {
//        Serial.print(button.name);
//        Serial.println(" button is released");
        Gamepad.release(button.pin);
        Gamepad.write();
      }
    }
  }
}
