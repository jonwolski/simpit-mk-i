# Simpit Mk I

USB HID for playing Kerbal Space Program

This may use <https://kerbalsimpit-arduino.readthedocs.io/en/stable/quickstart.html> or it may just be a USB HID.

Other decisions are captured in [`doc/adr/`](./doc/adr/)

![FreeCAD Screen Shot](doc/9-assembly-screen-shot.png)

[[_TOC_]]

## Physical controllers

| Action    | Controller Type         |
|-----------|-------------------------|
| Abort Arm | Switch w/ guard and LED |
| Abort     | Momentary button w/ LED |
| Stage Arm | Switch w/ guard and LED |
| Stage     | Momentary button w/ LED |
| SAS       | Toggle switch           |
| RCS       | Toggle switch           |
| Lights    | Toggle switch           |
| Gear      | Toggle switch           |
| Brakes    | Toggle switch           |

The momentary buttons have _integrated_ LEDs as do the toggle switches with
trigger guards. The other toggle switches will have corresponding LEDs for
'ON' position and will be illuminated when the trigger state is 'ON' and the
corresponding game state is also 'ON.'

Toggle switches will need to be synchronized with game state. When they are
out-of-sync, there are three options:

1. Update the game state so that it matches the physical controller state
2. Blink the physical controller state to warn the operator that it is out-of-sync
3. Ignore this. Do not handle physical controller input until it passes
   through the current game state.

## Tools

I used [FreeCAD](https://www.freecadweb.org/) for modeling and [CURA](https://ultimaker.com/software/ultimaker-cura)
for slicing and an [Ender 3 (v1)](https://creality3d.shop/collections/ender-series-3d-printer/products/creality-ender-3-3d-printer-economic-ender-diy-kits-with-resume-printing-function-v-slot-prusa-i3-220x220x250mm) for printing.

I learned the essentials for FreeCAD from ["FreeCAD for Beginners"](https://www.youtube.com/playlist?list=PLxa9m2nC6N924jFUOYRECQUMm9xl4_jUI), specifically lessons 2, 3, 4, 6, and 8.

## Materials

### Controller

![Arduino Leonardo](https://images-na.ssl-images-amazon.com/images/I/71b6ky5ho3L._AC_SL400_.jpg)

["Arduino Leonardo with Headers (A000057)"](https://www.amazon.com/gp/product/B008A36R2Y)

### wire

![wire](https://images-na.ssl-images-amazon.com/images/I/71iE4MOYX6L._AC_SL400_.jpg)

["22 awg Solid wire kit Electrical wire Cable 7colors 26ft each spools 22 gauge UL1007 Tinned Copper Hook up wire kit breadboard wire for DIY"](https://www.amazon.com/gp/product/B083DN5R61/)

### LEDs

![LEDs](https://images-na.ssl-images-amazon.com/images/I/71A8%2BAGmM1L._AC_SL400_.jpg)

["Chanzon 60 pcs(6 colors x 10 pcs) 3mm LED Diode Lights Assortment Kit Pack (Diffused Round Lens DC 3V 20mA) Lighting Bulb Lamp Assorted Variety Color Electronics Components Light Emitting Diodes Parts"](https://www.amazon.com/dp/B01C3W6NSK)

### LED mounts

![LED Mounts](https://images-na.ssl-images-amazon.com/images/I/61hvNxYPQCL._AC_SL400_.jpg)

["E-outstanding 25-Pack Copper 3mm Light Emitting Diode LED Bezel Holder - Round Lamp Emitting Diode Mount Panel Display Socket Base"](https://www.amazon.com/gp/product/B07D9HCNDX)


### Toggle Switches

![Toggle switch with red LED and trigger guard](https://images-na.ssl-images-amazon.com/images/I/612dNGaFbML._AC_SL400_.jpg)

["mxuteuk 2pcs Rocker Lighted Toggle Switch, 12V 20A Red LED Light up Toggle Switch Heavy Duty with Red Waterproof Cover SPST ON/Off 3Pin for Car Truck Boat ASW-07D-R-RMZ"](https://www.amazon.com/gp/product/B081DJ295D)


![Toggle switch with yellow LED and trigger guard](https://images-na.ssl-images-amazon.com/images/I/61TAMxvAGNL._AC_SL400_.jpg)

["mxuteuk 2pcs Rocker Lighted Toggle Switch, 12V 20A Yellow LED Light Up Toggle Switch Heavy Duty with Yellow Waterproof Cover SPST ON/Off 3Pin for Car Truck Boat ASW-07D-Y-YMZ"](https://www.amazon.com/gp/product/B081DJJP85)

![Mini toggle switch](https://images-na.ssl-images-amazon.com/images/I/61sF4cx2soL._AC_SL400_.jpg)

["ESUPPORT On/Off Mini Miniature Toggle Switch Car Dash Dashboard SPST 2Pin Blue Pack of 10"](https://www.amazon.com/gp/product/B01M3261RL)

### Buttons

![button with blue LED](https://images-na.ssl-images-amazon.com/images/I/51jicKtMUQL._AC_SL400_.jpg)

["Ulincos Momentary Push Button Switch U16F1 1NO1NC Silver Stainless Steel Shell with 12V Blue LED Ring Suitable for 16mm 5/8" Mounting Hole (Blue)"](https://www.amazon.com/gp/product/B06XT1MXBT)

![button with red LED](https://images-na.ssl-images-amazon.com/images/I/51G-S4G%2BVHL._AC_SL400_.jpg)

["Ulincos Momentary Push Button Switch U16F1 1NO1NC Silver Stainless Steel Shell with 12V Red LED Ring Suitable for 16mm 5/8" Mounting Hole (Red)"](https://www.amazon.com/gp/product/B06XSPCC9P)


